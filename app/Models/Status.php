<?php

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

    protected $table = 'status';
    protected $fillable = array('status');
    public $timestamps = false;

    public function run()
    {
        return $this->hasMany('Run');
    }

    public static function updateStatus()
    {
        $run = Run::find(Request::get('pk'));
        $run->status_id = Request::get('value');
        $run->save();
    }
}