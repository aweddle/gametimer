<?php

use Illuminate\Database\Eloquent\Model;

class Run extends Model {

    protected $table = 'runs';
    protected $fillable = array('game_id', 'name', 'status_id');
    public $timestamps = false;

    public function game()
    {
        return $this->belongsTo('Game');
    }

    public function segments()
    {
        return $this->hasMany('Segment');
    }

    public function status()
    {
        return $this->belongsTo('Status');
    }

    public function getElapsedTimeAttribute()
    {
        $elapsed = 0;

        foreach($this->segments as $segment)
        {
            $elapsed += Segment::toMilliseconds($segment->time_elapsed);
        }

        return Segment::formatTime($elapsed);
    }

    public function scopeInProgress($query)
    {
        $status = Status::where('status', 'In Progress')->get();

        return $query->where('status_id', $status->first()->id);
    }

    public function scopeCompleted($query)
    {
        $status = Status::where('status', 'Completed')->get();

        return $query->where('status_id', $status->first()->id);
    }

    public function scopeQuit($query)
    {
        $status = Status::where('status', 'Quit')->get();

        return $query->where('status_id', $status->first()->id);
    }

    public function scopeWithoutQuit($query)
    {
        $status = Status::where('status', 'Quit')->get();

        return $query->where('status_id', '!=', $status->first()->id);
    }

    public function scopeGamesPlayed($query)
    {
        return $query->groupBy('game_id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($run)
        {
            $status = Status::where('status', 'In Progress')->get();

            $run->status_id = $status->first()->id;
        });
    }
}