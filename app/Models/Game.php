<?php

use Illuminate\Database\Eloquent\Model;

class Game extends Model {

    protected $table = 'games';
    protected $fillable = array('name');
    public $timestamps = false;

    public function runs()
    {
        return $this->hasMany('Run');
    }

    public function systems()
    {
        return $this->belongsToMany('System', 'games_systems');
    }

    public function scopeAlphabetized($query)
    {
        return $query->orderBy('name');
    }

    public static function boot()
    {
        parent::boot();

        static::created(function($game)
        {
            //Relate the game and the system it's on after the game is created
            $game->systems()->attach(Input::get('system'));
        });
    }

    public function getAllSegmentsAttribute()
    {
        $segments = 0;

        foreach($this->runs as $run)
        {
            $segments += $run->segments->count();
        }

        return $segments;
    }

    public function getTotalTimePlayedAttribute()
    {
        $time = 0;

        foreach($this->runs as $run)
        {
            foreach($run->segments as $segment)
            {
                $time += Segment::toMilliseconds($segment->time_elapsed);
            }
        }

        return $total = Segment::formatTime($time);
    }

    public static function getTotalTimePlayed()
    {
        $time = 0;

        foreach(Game::all() as $game)
        {
            foreach($game->runs as $run)
            {
                foreach($run->segments as $segment)
                {
                    $time += Segment::toMilliseconds($segment->time_elapsed);
                }
            }
        }

        return $total = Segment::formatTime($time);
    }
}