<?php

use Illuminate\Database\Eloquent\Model;

class Segment extends Model {

    protected $table = 'segments';
    protected $fillable = array('run_id', 'date', 'time_elapsed', 'details');
    public $timestamps = false;

    public function run()
    {
        return $this->belongsTo('Run');
    }

    public function getDateAttribute($value)
    {
        return date('m/d/Y', strtotime($value));
    }

    public static function formatTime($milliseconds)
    {
        $seconds = floor($milliseconds /  1000);
        $minutes = floor($seconds / 60);
        $hours = floor($minutes / 60);
        $seconds = $seconds % 60;
        $minutes = $minutes % 60;

        if($seconds <= 9)
        {
            $seconds = '0'.$seconds;
        }

        if($minutes <= 9)
        {
            $minutes = '0'.$minutes;
        }

        if($hours <= 9)
        {
            $hours = '0'.$hours;
        }

        return $hours.':'.$minutes.':'.$seconds;
    }

    public static function toMilliseconds($time)
    {
        $milli = 0;

        list($hour, $minute, $second) = explode(':', $time);
        $milli += floor($hour*3600000);
        $milli += floor($minute*60000);
        $milli += floor($second*1000);

        return $milli;
    }

    public static function create(array $attributes)
    {
        $segment = parent::create($attributes);
        $segment->time_elapsed = Segment::formatTime(Request::get('time_elapsed'));
        $segment->date = Carbon\Carbon::now();
        $segment->save();

        return $segment;
    }
}