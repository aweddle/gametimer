<?php

use Illuminate\Database\Eloquent\Model;

class System extends Model {

    protected $table = 'systems';
    protected $fillable = array('name', 'abbreviation');
    public $timestamps = false;

    public function games()
    {
        return $this->belongsToMany('Game', 'games_systems');
    }
}