<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

get('/', 'DashboardController@getIndex');

//Restful controllers
Route::controller('dashboard', 'DashboardController');
Route::controller('systems', 'SystemController');
Route::controller('games', 'GameController');
Route::controller('runs', 'RunController');
Route::controller('segments', 'SegmentController');
Route::controller('status', 'StatusController');
