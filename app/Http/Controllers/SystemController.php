<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Http\Requests\CreateSystemRequest;
use System;

class SystemController extends Controller
{
    public $data = [];

    public function getAdd()
    {
        return response()->json(array('status' => true, 'view' => view('systems.modal')->render()));
    }

    public function getIndex()
    {
        $this->data['systems'] = System::all();

        return view('systems.index', $this->data);
    }

    public function getProfile($system_id)
    {
        $this->data['system'] = System::find($system_id);

        return view('systems.profile', $this->data);
    }

    public function postAdd(CreateSystemRequest $request)
    {
        $system = System::create($request->all());

        return response()->json(array('status' => true, 'sys' => $system->toArray(), 'message' => 'Successfully added system to your collection!'));
    }
}