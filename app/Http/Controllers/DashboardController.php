<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Run, Game;

class DashboardController extends Controller
{
    public $data = [];

    public function getIndex()
    {
        $this->data['runs_completed']    = Run::completed()->get()->count();
        $this->data['runs_quit']         = Run::quit()->get()->count();
        $this->data['games_beaten']      = Run::completed()->gamesPlayed()->get()->count();
        $this->data['total_playtime']    = Game::getTotalTimePlayed();
        $this->data['total_runs']        = Run::all()->count();
        $this->data['total_games']       = Game::all()->count();

        return view('dashboard.index', $this->data);
    }
}