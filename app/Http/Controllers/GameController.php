<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Http\Requests\CreateGameRequest;
use System, Game;
use Reqest;

class GameController extends Controller
{
    public $data = [];

    public function getAdd()
    {
        $this->data['systems'] = System::lists('name', 'id');

        return response()->json(array('status' => true, 'view' => view('games.modal', $this->data)->render()));
    }

    public function getIndex()
    {
        $this->data['games'] = Game::all();

        return view('games.index', $this->data);
    }

    public function getProfile($gameID)
    {
        $this->data['game'] = Game::find($gameID);

        return view('games.profile', $this->data);
    }

    public function postAdd(CreateGameRequest $request)
    {
        $game = Game::create($request->all());

        return response()->json(array('status' => true, 'game' => $game->toArray(), 'systems' => $game->systems()->first()->toArray(), 'message' => 'Successfully added game to your collection!'));
    }

    public function postAddSystem($gameID)
    {
        $game = Game::find($gameID);
        $game->systems()->attach(Request::get('system_id'));

        return response()->json(array('status' => true));
    }
}