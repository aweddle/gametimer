<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Http\Requests\CreateSegmentRequest;
use Carbon\Carbon;
use Segment;

class SegmentController extends Controller
{
    public $data = [];

    public function getAdd($runID)
    {
        $this->data['run_id'] = $runID;

        return response()->json(array('status' => true, 'view' => view('segments.modal', $this->data)->render()));
    }

    public function postAdd(CreateSegmentRequest $request)
    {
        $segment = Segment::create($request->except('time_elapsed'));

        return response()->json(array('status' => true, 'segment' => $segment->toArray(), 'message' => 'Segment successfully recorded!'));
    }
}