<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Status;

class StatusController extends Controller
{
    public $data = [];

    public function getStatuses()
    {
        return response()->json(Status::lists('status', 'id'));
    }

    public function postUpdateStatus()
    {
        Status::updateStatus();
    }
}