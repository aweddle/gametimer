<?php namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use App\Http\Requests\CreateRunRequest;
use Run, Game;

class RunController extends Controller
{
    public $data = [];

    public function getAdd($gameID = null)
    {
        $this->data['game'] = $gameID;
        $this->data['games'] = Game::orderBy('name')->lists('name', 'id');

        return response()->json(array('status' => true, 'view' => view('runs.modal', $this->data)->render()));
    }

    public function getIndex()
    {
        $this->data['runs'] = Run::withoutQuit()->get()->reverse();

        return view('runs.index', $this->data);
    }

    public function getProfile($runID)
    {
        $this->data['run'] = Run::find($runID);

        return view('runs.profile', $this->data);
    }

    public function postAdd(CreateRunRequest $request)
    {
        $run = Run::create($request->all());

        return response()->json(array('status' => true, 'run' => $run->toArray(), 'message' => 'Successfully created run!'));
    }
}