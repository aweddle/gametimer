$('#esc_messages_table').dataTable({
    "iDisplayLength": 10,
    "aaSorting": [[0, 'desc']]
});
$(document).ready(function(){

    toastr.options.closebutton = true;

    $('#profile_submit').on('click', function(e){
        e.preventDefault();

        $.ajax({
            url: '/users/profile',
            type: 'POST',
            dataType: 'json',
            data:
            {
                _token:           $('input[name="_token"]').val(),
                id:               $('input[name="id"]').val(),
                first_name:       $('#first_name').val(),
                last_name:        $('#last_name').val(),
                email:            $('#email').val()
            },
            success: function(response)
            {
                if(response.status === true)
                {
                    toastr.success(response.message);
                }
                else
                {
                    $.each(response.message, function(index, element){
                        toastr.error(element);
                    });
                }
            }
        });
    });

});