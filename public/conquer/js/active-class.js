$(document).ready(function(){

    var url = window.location.pathname.split( '/' );

    switch(url[1])
    {
        case 'dashboard':
            $('#dashboard').addClass('start active');
            break;
        case 'systems':
            $('#systems').addClass('start active');
            break;
        case 'games':
            $('#games').addClass('start active');
            break;
        default:
            break;
    }
});