$('#runs_table').dataTable({
    "iDisplayLength": 10,
    "aaSorting": [[4, 'desc']]
});

$('#addModal').hide();
toastr.options.closeButton = true;
$.fn.editable.defaults.mode = 'popup';

$(document).ready(function()
{
    $('.status').editable({
        showbuttons: false
    });

    //Modal functions
    $("#closeModal").on('click', function(){
        $('.modal-body').empty();
        $('#addModal').hide();
    });

    $(".close").on('click', function(){
        $('.modal-body').empty();
        $('#addModal').hide();
    });

    $(".new_run").on('click', function(){

        $.ajax({
            url: '/runs/add',
            type: 'GET',
            dataType: 'json',
            success: function(response)
            {
                if(response.status == true)
                {
                    //Create model form here with returned model object and append to modal
                    $('.modal-body').empty().html(response.view);
                }
                else
                {
                    toastr.error(response.message);
                }
            }
        });
        $('#addModal').show();
    });

    $('#submitModal').on('click', function()
    {
        $.ajax({
            url: '/runs/add',
            type: 'POST',
            dataType: 'json',
            data:
            {
                _token:   $('input[name="_token"]').val(),
                name:     $('#name').val(),
                game_id:  $('#game').val()
            },
            success: function(response)
            {
                if(response.status == true)
                {
                    var string = "<tr><td>"+response.run.name+"</td><td>0</td><td>In Progress</td></tr>";

                    $('#run_body').append(string);
                    toastr.success(response.message);

                    //Close the modal and empty the form
                    $('.modal-body').empty();
                    $('#addModal').hide();
                }
                else
                {
                    $.each(response.message, function(index, element){
                        toastr.error(element);
                    });
                }
            }
        })
    });
});
