$('#profile_table').dataTable({
    "iDisplayLength": 30,
    "aaSorting": [[0, 'asc']]
});

$('.addModal').hide();
toastr.options.closeButton = true;

$(document).ready(function()
{
    //Modal functions
    $("#closeModal").on('click', function(){
        $('.modal-body').empty();
        $('.addModal').hide();
    });

    $(".close").on('click', function(){
        $('.modal-body').empty();
        $('.addModal').hide();
    });

    $(".new_run").on('click', function(){
        //Get the ID of the game whose profile we're on
        var id = $(this).attr('id');

        $.ajax({
            url: '/runs/add/'+id,
            type: 'GET',
            dataType: 'json',
            success: function(response)
            {
                if(response.status == true)
                {
                    //Create model form here with returned model object and append to modal
                    $('.modal-body').empty().html(response.view);
                }
                else
                {
                    toastr.error(response.message);
                }
            }
        });
        $('#addRunModal').show();
    });

    $('#submitRunModal').on('click', function()
    {
        $.ajax({
            url: '/runs/add',
            type: 'POST',
            dataType: 'json',
            data:
            {
                _token:   $('input[name="_token"]').val(),
                name:     $('#name').val(),
                game_id:  $('#game').val()
            },
            success: function(response)
            {
                if(response.status == true)
                {
                    var string = "<tr><td>"+response.run.name+"</td><td>0</td><td>In Progress</td></tr>";

                    $('#run_body').append(string);
                    toastr.success(response.message);
                }
                else
                {
                    toastr.error(response.message);
                }

                //Close the modal and empty the form
                $('.modal-body').empty();
                $('#addSysModal').hide();
            }
        })
    });

    $(".new_system").on('click', function(){

        $.ajax({
            url: '/games/add',
            type: 'GET',
            dataType: 'json',
            success: function(response)
            {
                if(response.status == true)
                {
                    //Create model form here with returned model object and append to modal
                    $('.modal-body').empty().html(response.view);
                }
                else
                {
                    toastr.error(response.message);
                }
            }
        });
        $('#addSysModal').show();
    });

    $('#submitSysModal').on('click', function()
    {
        $.ajax({
            url: '/runs/add',
            type: 'POST',
            dataType: 'json',
            data:
            {
                name:     $('#name').val(),
                game_id:  $('#game').val()
            },
            success: function(response)
            {
                if(response.status == true)
                {
                    var string = "<tr><td>"+response.run.name+"</td><td>0</td><td>In Progress</td></tr>";

                    $('#run_body').append(string);
                    toastr.success(response.message);
                }
                else
                {
                    toastr.error(response.message);
                }

                //Close the modal and empty the form
                $('.modal-body').empty();
                $('#addSysModal').hide();
            }
        })
    });
});
