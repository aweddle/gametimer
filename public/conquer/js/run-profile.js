$('#profile_table').dataTable({
    "iDisplayLength": 10,
    "aaSorting": [[0, 'desc']]
});
$('#addModal').hide();
toastr.options.closeButton = true;

$(document).ready(function()
{
    var date;
    var elapsed;
    var modal = $('#addModal');
    var counter = 0;


    //Janky timer functionality inc
    modal.on('click', '.start', function()
    {
        if(counter == 0)
        {
            date = $.now();
            counter++;

            $('.start').empty().append('Stop Timer');
        }
        else
        {
            var date2 = $.now();
            elapsed = date2 - date;
            counter--;

            $('.start').attr('disabled', true);
        }
    });

    //Need to figure out better way to make this work
//    modal.on('click', '.start', function()
//    {
//        date = $.now();
//    });
//
//    modal.on('click', '.stop', function()
//    {
//        elapsed += $.now() - date;
//    });

    //Modal functions
    $("#closeModal").on('click', function(){
        $('.modal-body').empty();
        $('#addModal').hide();
    });

    $(".close").on('click', function(){
        $('.modal-body').empty();
        $('#addModal').hide();
    });

    $(".new_segment").on('click', function(){
        //Get the ID of the run whose profile we're on
        var id = $(this).attr('id');

        $.ajax({
            url: '/segments/add/'+id,
            type: 'GET',
            dataType: 'json',
            success: function(response)
            {
                if(response.status == true)
                {
                    //Create model form here with returned model object and append to modal
                    $('.modal-body').empty().html(response.view);
                }
                else
                {
                    toastr.error(response.message);
                }
            }
        });
        $('#addModal').show();
    });

    $('#submitModal').on('click', function()
    {
        $.ajax({
            url: '/segments/add',
            type: 'POST',
            dataType: 'json',
            data:
            {
                _token:       $('input[name="_token"]').val(),
                run_id:       $('input[name="run_id"]').val(),
                time_elapsed: elapsed,
                details:      $('#details').val()
            },
            success: function(response)
            {
                if(response.status == true)
                {
                    var string = "<tr><td>"+response.segment.date+"</td><td>"+response.segment.time_elapsed+"</td><td>"+response.segment.details+"</td></tr>";

                    $('#segment_body').prepend(string);
                    toastr.success(response.message);

                    //Close the modal and empty the form
                    $('.modal-body').empty();
                    $('#addModal').hide();
                }
                else
                {
                    $.each(response.message, function(index, element){
                        toastr.error(element);
                    });
                }
            }
        })
    });
});
