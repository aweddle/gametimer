$('#games_table').dataTable({
    "iDisplayLength": 10,
    "aaSorting": [[4, 'desc']]
});
$('#addModal').hide();
toastr.options.closeButton = true;

$(document).ready(function()
{
    //Modal functions
    $("#closeModal").on('click', function(){
        $('.modal-body').empty();
        $('#addModal').hide();
    });

    $(".close").on('click', function(){
        $('.modal-body').empty();
        $('#addModal').hide();
    });

    $("#add_game").on('click', function(){
        $.ajax({
            url: '/games/add',
            type: 'GET',
            dataType: 'json',
            success: function(response)
            {
                if(response.status == true)
                {
                    //Create model form here with returned model object and append to modal
                    $('.modal-body').empty().html(response.view);
                }
                else
                {
                    toastr.error(response.message);
                }
            }
        });
        $('#addModal').show();
    });

    $('#submitModal').on('click', function()
    {
        $.ajax({
            url: '/games/add',
            type: 'POST',
            dataType: 'json',
            data:
            {
                _token:  $('input[name="_token"]').val(),
                name:    $('#name').val(),
                system:  $('#system').val()
            },
            success: function(response)
            {
                if(response.status == true)
                {
                    var string = "<tr><td>"+response.game.name+"</td><td>"+response.systems.name+"</td><td>0</td><td>0</td><td>0</td></tr>";

                    $('#game_body').append(string);
                    toastr.success(response.message);

                    //Close the modal and empty the form
                    $('.modal-body').empty();
                    $('#addModal').hide();
                }
                else
                {
                    $.each(response.message, function(index, element){
                        toastr.error(element);
                    });
                }
            }
        })
    });
});