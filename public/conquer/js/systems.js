$('#systems_table').dataTable({
    "iDisplayLength": 30,
    "aaSorting": [[0, 'asc']]
});
$('#addModal').hide();
toastr.options.closeButton = true;

$(document).ready(function()
{
    //Modal functions
    $("#closeModal").on('click', function(){
        $('.modal-body').empty();
        $('#addModal').hide();
    });

    $(".close").on('click', function(){
        $('.modal-body').empty();
        $('#addModal').hide();
    });

    $("#add_system").on('click', function(){
        $.ajax({
            url: '/systems/add',
            type: 'GET',
            dataType: 'json',
            success: function(response)
            {
                if(response.status == true)
                {
                    //Create model form here with returned model object and append to modal
                    $('.modal-body').empty().html(response.view);
                }
                else
                {
                    toastr.error(response.message);
                }
            }
        });
        $('#addModal').show();
    });

    $('#submitModal').on('click', function()
    {
        $.ajax({
            url: '/systems/add',
            type: 'POST',
            dataType: 'json',
            data:
            {
                _token:       $('input[name="_token"]').val(),
                name:         $('#name').val(),
                abbreviation: $('#abr').val()
            },
            success: function(response)
            {
                if(response.status == true)
                {
                    var string = "<tr><td>"+response.sys.name+"</td><td>"+response.sys.abbreviation+"</td><td></td></tr>";

                    $('#system_body').append(string);
                    toastr.success(response.message);

                    //Close the modal and empty the form
                    $('.modal-body').empty();
                    $('#addModal').hide();
                }
                else
                {
                    $.each(response.message, function(index, element){
                        toastr.error(element);
                    });
                }
            }
        })
    });
});