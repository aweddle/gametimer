<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('SystemSeeder');
        $this->command->info('Systems seeded successfully!');

        $this->call('StatusSeeder');
        $this->command->info('Statuses seeded successfully!');
	}

}
