<?php

use Illuminate\Database\Seeder;

class SystemSeeder extends Seeder
{
    public $systems = array(
        array('name' => 'Nintendo Entertainment System', 'abbreviation' => 'NES'),
        array('name' => 'Super Nintendo', 'abbreviation' => 'SNES'),
        array('name' => 'Nintendo 64', 'abbreviation' => 'N64'),
        array('name' => 'Nintendo Gamecube', 'abbreviation' => 'GCN'),
        array('name' => 'Nintendo Wii', 'abbreviation' => 'Wii'),
        array('name' => 'Nintendo WiiU', 'abbreviation' => 'WiiU'),
        array('name' => 'Nintendo Gameboy', 'abbreviation' => 'GB'),
        array('name' => 'Nintendo Gameboy Color', 'abbreviation' => 'GBC'),
        array('name' => 'Nintendo Gameboy Advance', 'abbreviation' => 'GBA'),
        array('name' => 'Nintendo DS', 'abbreviation' => 'DS'),
        array('name' => 'Nintendo 3DS', 'abbreviation' => '3DS'),
        array('name' => 'Playstation', 'abbreviation' => 'PSX'),
        array('name' => 'Playstation 2', 'abbreviation' => 'PS2'),
        array('name' => 'Playstation 4', 'abbreviation' => 'PS4'),
        array('name' => 'Playstation Vita', 'abbreviation' => 'Vita'),
        array('name' => 'Personal Computer', 'abbreviation' => 'PC'));

    public function run()
    {
        //Clear the table
        DB::table('systems')->delete();

        //Seed it!
        foreach($this->systems as $system)
        {
            System::create($system);
        }
    }
}