<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    public $status = array(
        array('status' => 'In Progress'),
        array('status' => 'Completed'),
        array('status' => 'Quit')
    );

    public function run()
    {
        //Clean it
        DB::table('status')->delete();

        //Seed it
        foreach($this->status as $status)
        {
            Status::create($status);
        }
    }
}