<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBaseTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('games_systems', function(Blueprint $table)
        {
            $table->integer('game_id');
            $table->integer('system_id');
        });

        Schema::table('games', function(Blueprint $table)
        {
            $table->dropColumn('system_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('games_systems');

        Schema::table('games', function(Blueprint $table)
        {
            $table->integer('system_id');
        });
	}

}
