<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('games', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('system_id');
            $table->string('name');
        });

        Schema::create('systems', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('abbreviation');
        });

        Schema::create('runs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('game_id');
            $table->string('name');
            $table->integer('status_id');
        });

        Schema::create('segments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('run_id');
            $table->date('date');
            $table->time('time_elapsed');
            $table->string('details');
        });

        Schema::create('status', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('status');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('games');
        Schema::drop('systems');
        Schema::drop('runs');
        Schema::drop('segments');
        Schema::drop('status');
	}

}
