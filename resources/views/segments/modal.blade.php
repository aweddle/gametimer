{!! Form::open() !!}
{!! Form::hidden('run_id', $run_id, array('id' => 'run_id')) !!}
<div class="row">
    <div class="col-md-12" style="padding-bottom: 10px;">
        <button class="btn btn-success start" type="button">Start Timer</button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {!! Form::label('details', 'Segment Details') !!}
        {!! Form::textarea('details', null, array('class' => 'form-control', 'id' => 'details')) !!}
    </div>
</div>
{!! Form::close() !!}