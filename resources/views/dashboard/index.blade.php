@extends('layouts.default')

@section('title', 'GameTimer | Dashboard')

@section('css')
{!! HTML::style('conquer/plugins/data-tables/DT_bootstrap.css') !!}
@stop

@section('page-title')
Dashboard
@overwrite

@section('main')
<div class="row stats-overview-cont">
    <div class="col-md-3">
        <div class="stats-overview stat-block">
            <div class="display stat good huge">
                <span class="line-chart" style="display: inline;">
                <span style="display: none;">
                <span style="display: none;">
                <span style="display: none;">2,6,8,12, 11, 15, 16, 11, 16, 11, 10, 3, 7, 8, 12, 19 </span>
                <canvas width="50" height="20"></canvas>
                </span>
                <canvas width="50" height="20"></canvas>
                </span>
                <canvas width="40" height="20"></canvas>
                </span>
                <div class="percent">100%</div>
            </div>
            <div class="details">
                <div class="title">Time Played:</div>
                <div class="numbers">{{$total_playtime}}</div>
            </div>
            <div class="progress">
                <span class="progress-bar progress-bar-success" aria-valuemax="100" aria-valuemin="0" aria-valuenow="100" style="width: 100%;">
                    <span class="sr-only"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="stats-overview stat-block">
            <div class="display stat good huge">
                <span class="line-chart" style="display: inline;">
                <span style="display: none;">
                <span style="display: none;">
                <span style="display: none;">2,6,8,12, 11, 15, 16, 11, 16, 11, 10, 3, 7, 8, 12, 19 </span>
                <canvas width="50" height="20"></canvas>
                </span>
                <canvas width="50" height="20"></canvas>
                </span>
                <canvas width="40" height="20"></canvas>
                </span>
                <div class="percent">{{ $total_runs ? round($runs_completed/$total_runs*100, 2) : 0 }}%</div>
            </div>
            <div class="details">
                <div class="title">Runs Completed:</div>
                <div class="numbers">{{$runs_completed}}</div>
            </div>
            <div class="progress">
                <span class="progress-bar progress-bar-success" aria-valuemax="100" aria-valuemin="0" aria-valuenow="{{ $total_runs ? round($runs_completed/$total_runs, 2) : 0 }}" style="width: <?php echo $total_runs ? round($runs_completed/$total_runs*100, 2) : 0;?>%;">
                    <span class="sr-only"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="stats-overview stat-block">
            <div class="display stat bad huge">
                <span class="line-chart" style="display: inline;">
                <span style="display: none;">
                <span style="display: none;">
                <span style="display: none;">2,6,8,12, 11, 15, 16, 11, 16, 11, 10, 3, 7, 8, 12, 19 </span>
                <canvas width="50" height="20"></canvas>
                </span>
                <canvas width="50" height="20"></canvas>
                </span>
                <canvas width="40" height="20"></canvas>
                </span>
                <div class="percent">{{ $total_runs ? round($runs_quit/$total_runs*100, 2) : 0 }}%</div>
            </div>
            <div class="details">
                <div class="title">Runs Quit:</div>
                <div class="numbers">{{$runs_quit}}</div>
            </div>
            <div class="progress">
                <span class="progress-bar progress-bar-danger" aria-valuemax="100" aria-valuemin="0" aria-valuenow="{{ $total_runs ? round($runs_quit/$total_runs, 2) : 0}}" style="width: <?php echo $total_runs ? round($runs_quit/$total_runs*100, 2) : 0;?>%;">
                    <span class="sr-only"></span>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="stats-overview stat-block">
            <div class="display stat good huge">
                <span class="line-chart" style="display: inline;">
                <span style="display: none;">
                <span style="display: none;">
                <span style="display: none;">2,6,8,12, 11, 15, 16, 11, 16, 11, 10, 3, 7, 8, 12, 19 </span>
                <canvas width="50" height="20"></canvas>
                </span>
                <canvas width="50" height="20"></canvas>
                </span>
                <canvas width="40" height="20"></canvas>
                </span>
                <div class="percent">{{ $total_games ? round($games_beaten/$total_games*100, 2) : 0 }}%</div>
            </div>
            <div class="details">
                <div class="title">Games Beaten:</div>
                <div class="numbers">{{$games_beaten}}</div>
            </div>
            <div class="progress">
                <span class="progress-bar progress-bar-success" aria-valuemax="100" aria-valuemin="0" aria-valuenow="{{ $total_games ? round($games_beaten/$total_games, 2) : 0 }}" style="width: <?php echo $total_games ? $games_beaten/$total_games*100 : 0;?>%;">
                    <span class="sr-only"></span>
                </span>
            </div>
        </div>
    </div>
</div>
@stop

@section('jquery2')
{!! HTML::script('conquer/plugins/data-tables/jquery.dataTables.js') !!}
@stop

@section('scripts')
{!! HTML::script('conquer/plugins/data-tables/DT_bootstrap.js') !!}
@stop