
<div class="page-sidebar-wrapper">
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            <ul class="page-sidebar-menu">
                <li class="sidebar-toggler-wrapper">
                    <div class="sidebar-toggler">
                    </div>
                    <div class="clearfix">
                    </div>
                </li>
                <li id ='dashboard' class="">
                    <a href="{{ URL::to('/dashboard') }}">
                        <i class="fa fa-home"></i>
					<span class="title">
						Dashboard
					</span>
                    </a>
                </li>
                <li id ='systems' class="">
                    <a href="{{ URL::to('/systems') }}">
                        <i class="fa fa-home"></i>
					<span class="title">
						Systems
					</span>
                    </a>
                </li>
                <li id ='games' class="">
                    <a href="{{ URL::to('/games') }}">
                        <i class="fa fa-home"></i>
					<span class="title">
						Games
					</span>
                    </a>
                </li>
                <li id ='runs' class="">
                    <a href="{{ URL::to('/runs') }}">
                        <i class="fa fa-home"></i>
					<span class="title">
						Runs
					</span>
                    </a>
                </li>
                <li class="devider">
                    &nbsp;
                </li>
            </ul>
        </div>
    </div>
</div>