<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<head>
    <title>@yield('title')</title>
    {!! HTML::style('conquer/plugins/font-awesome/css/font-awesome.min.css') !!}
    {!! HTML::style('conquer/plugins/bootstrap/css/bootstrap.min.css') !!}
    {!! HTML::style('conquer/plugins/uniform/css/uniform.default.css') !!}
    {!! HTML::style('conquer/css/style-conquer.css') !!}
    {!! HTML::style('conquer/css/style.css') !!}
    {!! HTML::style('conquer/css/style-responsive.css') !!}
    {!! HTML::style('conquer/css/plugins.css') !!}
    {!! HTML::style('conquer/css/themes/default.css') !!}
    {!! HTML::style('conquer/css/custom.css') !!}
    {!! HTML::style('conquer/plugins/bootstrap-fileupload/bootstrap-fileupload.css') !!}
    {!! HTML::style('conquer/plugins/bootstrap-toastr/toastr.css') !!}
    @yield('css')
    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<body class="page-header-fixed">
<div class="header navbar navbar-inverse navbar-fixed-top">
    @include('layouts/top-nav')
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('layouts/side-menu')
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="page-title">
                            @section('page-title')
                            Dashboard <small>statistics and more</small>
                            @show
                        </h3>
                    </div>
                </div>
                <div>
                    @yield('main')
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="footer-inner">© <?= date('Y')?> Aaron Weddle</div>
        <div class="footer-tools">
            <span class="go-top">
                <i class="fa fa-angle-up"></i>
            </span>
        </div>
    </div>
    @yield('jquery1')
    {!! HTML::script('conquer/plugins/jquery-1.10.2.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery-migrate-1.2.1.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') !!}
    {!! HTML::script('conquer/plugins/bootstrap/js/bootstrap.min.js') !!}
    {!! HTML::script('conquer/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery-slimscroll/jquery.slimscroll.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery.blockui.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery.cokie.min.js') !!}
    {!! HTML::script('conquer/plugins/uniform/jquery.uniform.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery.peity.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery.pulsate.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery-knob/js/jquery.knob.js') !!}
    {!! HTML::script('conquer/plugins/flot/jquery.flot.js') !!}
    {!! HTML::script('conquer/plugins/flot/jquery.flot.pie.min.js') !!}
    {!! HTML::script('conquer/plugins/flot/jquery.flot.resize.js') !!}
    {!! HTML::script('conquer/plugins/bootstrap-daterangepicker/moment.min.js') !!}
    {!! HTML::script('conquer/plugins/bootstrap-daterangepicker/daterangepicker.js') !!}
    {!! HTML::script('conquer/plugins/gritter/js/jquery.gritter.js') !!}
    {!! HTML::script('conquer/plugins/fullcalendar/fullcalendar/fullcalendar.min.js') !!}
    {!! HTML::script('conquer/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js') !!}
    {!! HTML::script('conquer/plugins/jquery.sparkline.min.js') !!}
    {!! HTML::script('conquer/plugins/bootstrap-toastr/toastr.js') !!}
    @yield('jquery2')

    {!! HTML::script('conquer/scripts/app.js') !!}
    {!! HTML::script('conquer/scripts/index.js') !!}
    {!! HTML::script('conquer/scripts/tasks.js') !!}

    {!! HTML::script('conquer/plugins/bootstrap-fileupload/bootstrap-fileupload.js') !!}
    {!! HTML::script('conquer/js/active-class.js') !!}
    @yield('scripts')
    <script>
        jQuery(document).ready(function() {
            App.init(); // initlayout and core plugins
            Index.init();
            Index.initJQVMAP(); // init index page's custom scripts
            Index.initCalendar(); // init index page's custom scripts
            Index.initCharts(); // init index page's custom scripts
            Index.initChat();
            Index.initMiniCharts();
            Index.initPeityElements();
            Index.initKnowElements();
            Index.initDashboardDaterange();
            Tasks.initDashboardWidget();
        });
    </script>
</body>
</html>