@extends('layouts.default')

@section('title', 'GameTimer | Systems')

@section('css')
{!! HTML::style('conquer/plugins/data-tables/DT_bootstrap.css') !!}
@stop

@section('page-title')
Systems
@overwrite

@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Systems</div><div class="pull-right"><a class="btn btn-xs btn-info" id="add_system"><i class="fa fa-plus"></i>  Add System</a></div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table id="systems_table" class="table table-striped table-hover dataTable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Abbreviation</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="system_body">
                        @foreach($systems as $system)
                            <tr id="{{$system->id}}">
                                <td><a href="/systems/profile/{{$system->id}}">{{$system->name}}</a></td>
                                <td>{{$system->abbreviation}}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!---Add System Modal--->
<div id="addModal" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                <h4 class="modal-title">Add System</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="closeModal" class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                <button id="submitModal" class="btn btn-info" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('jquery2')
{!! HTML::script('conquer/plugins/data-tables/jquery.dataTables.js') !!}
@stop

@section('scripts')
{!! HTML::script('conquer/plugins/data-tables/DT_bootstrap.js') !!}
{!! HTML::script('conquer/js/systems.js') !!}
@stop