{!! Form::open() !!}
<div class="row">
    <div class="col-md-12">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, array('class' => 'form-control', 'id' => 'name')) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {!! Form::label('abr', 'Abbreviation') !!}
        {!! Form::text('abr', null, array('class' => 'form-control', 'id' => 'abr')) !!}
    </div>
</div>
{!! Form::close() !!}