@extends('layouts.default')

@section('title', 'GameTimer | System Profile')

@section('css')
{!! HTML::style('conquer/plugins/data-tables/DT_bootstrap.css') !!}
@stop

@section('page-title')
{{ $system->name }}
@overwrite

@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Games</div><div class="pull-right"><a class="btn btn-xs btn-info" id="add_game"><i class="fa fa-plus"></i>  Add Game</a></div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table id="games_table" class="table table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>System</th>
                            <th>Runs</th>
                            <th>Segments</th>
                            <th>Total Time Played</th>
                            <th>Runs Completed</th>
                        </tr>
                        </thead>
                        <tbody id="game_body">
                        @foreach($system->games as $game)
                        <tr id="{{$game->id}}">
                            <td><a href="/games/profile/{{$game->id}}">{{$game->name}}</a></td>
                            @if($game->systems->count() > 1)
                            @foreach($game->systems as $system)
                            <td>{{$system->abbreviation}} </td>
                            @endforeach
                            @else
                            @foreach($game->systems as $system)
                            <td>{{$system->name}}</td>
                            @endforeach
                            @endif
                            <td>{{$game->runs->count()}}</td>
                            <td>{{$game->all_segments}}</td>
                            <td>{{$game->total_time_played}}</td>
                            <td>{{$game->runs()->completed()->get()->count()}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!---Add System Modal--->
<div id="addModal" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                <h4 class="modal-title">Add Game</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="closeModal" class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                <button id="submitModal" class="btn btn-info" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('jquery2')
{!! HTML::script('conquer/plugins/data-tables/jquery.dataTables.js') !!}
@stop

@section('scripts')
{!! HTML::script('conquer/plugins/data-tables/DT_bootstrap.js') !!}
{!! HTML::script('conquer/js/system-profile.js') !!}
@stop