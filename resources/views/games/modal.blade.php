{!! Form::open() !!}
<div class="row">
    <div class="col-md-12">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, array('class' => 'form-control', 'id' => 'name')) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {!! Form::label('system', 'System') !!}
        {!! Form::select('system', $systems, null, array('class' => 'form-control', 'id' => 'system')) !!}
    </div>
</div>
{!! Form::close() !!}