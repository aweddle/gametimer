@extends('layouts.default')

@section('title', 'GameTimer | Games')

@section('css')
{!! HTML::style('conquer/plugins/data-tables/DT_bootstrap.css') !!}
@stop

@section('page-title')
{{ $game->name }}
@overwrite

@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Systems</div><div class="pull-right"><a class="btn btn-xs btn-info new_system" id="{{$game->id}}"><i class="fa fa-plus"></i>  New System</a></div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table id="system_table" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>System</th>
                            <th>Abbreviation</th>
                        </tr>
                        </thead>
                        <tbody id="system_body">
                        @foreach($game->systems as $system)
                        <tr id="{{$system->id}}">
                            <td><a href="/systems/profile/{{$system->id}}">{{$system->name}}</a></td>
                            <td>{{$system->abbreviation}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Runs</div><div class="pull-right"><a class="btn btn-xs btn-info new_run" id="{{$game->id}}"><i class="fa fa-plus"></i>  New Run</a></div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table id="profile_table" class="table table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Run</th>
                            <th>Segments</th>
                            <th>Time Elapsed</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody id="run_body">
                        @foreach($game->runs as $run)
                        <tr id="{{$run->id}}">
                            <td><a href="/runs/profile/{{$run->id}}">{{$run->name}}</a></td>
                            <td>{{$run->segments->count()}}</td>
                            <td>{{$run->elapsed_time}}</td>
                            <td>{{$run->status->status}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!---Add Run Modal--->
<div id="addRunModal" class="modal fade in addModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                <h4 class="modal-title">New Run</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="closeModal" class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                <button id="submitRunModal" class="btn btn-info" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
<div id="addSysModal" class="modal fade in addModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                <h4 class="modal-title">New System</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="closeModal" class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                <button id="submitSysModal" class="btn btn-info" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('jquery2')
{!! HTML::script('conquer/plugins/data-tables/jquery.dataTables.js') !!}
@stop

@section('scripts')
{!! HTML::script('conquer/plugins/data-tables/DT_bootstrap.js') !!}
{!! HTML::script('conquer/js/game-profile.js') !!}
@stop