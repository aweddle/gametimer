@extends('layouts.default')

@section('title', 'GameTimer | Runs')

@section('css')
{!! HTML::style('conquer/plugins/data-tables/DT_bootstrap.css') !!}
@stop

@section('page-title')
{{ $run->name }}
@overwrite

@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Segments</div><div class="pull-right"><a class="btn btn-xs btn-info new_segment" id="{{$run->id}}"><i class="fa fa-plus"></i>  New Segment</a></div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table id="profile_table" class="table table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Details</th>
                        </tr>
                        </thead>
                        <tbody id="segment_body">
                        @foreach($run->segments->reverse() as $segment)
                        <tr id="{{$segment->id}}">
                            <td>{{$segment->date}}</a></td>
                            <td>{{$segment->time_elapsed}}</td>
                            <td>{{$segment->details}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!---Add Segment Modal--->
<div id="addModal" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                <h4 class="modal-title">New Segment</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="closeModal" class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                <button id="submitModal" class="btn btn-info" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('jquery2')
{!! HTML::script('conquer/plugins/data-tables/jquery.dataTables.js') !!}
@stop

@section('scripts')
{!! HTML::script('conquer/plugins/data-tables/DT_bootstrap.js') !!}
{!! HTML::script('conquer/js/run-profile.js') !!}
@stop