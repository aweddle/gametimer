@extends('layouts.default')

@section('title', 'GameTimer | Runs')

@section('css')
{!! HTML::style('conquer/plugins/data-tables/DT_bootstrap.css') !!}
{!! HTML::style('conquer/plugins/bootstrap-editable/bootstrap-editable/css/bootstrap-editable.css') !!}
@stop

@section('page-title')
Runs
@overwrite

@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption">Runs</div><div class="pull-right"><a class="btn btn-xs btn-info new_run"><i class="fa fa-plus"></i>  Add Run</a></div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table id="runs_table" class="table table-striped table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Game</th>
                            <th>Segments</th>
                            <th>Total Elapsed Time</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody id="run_body">
                        @foreach($runs as $run)
                        <tr id="{{$run->id}}">
                            <td><a href="/runs/profile/{{$run->id}}">{{$run->name}}</a></td>
                            <td><a href="/games/profile/{{$run->game->id}}">{{$run->game->name}}</a></td>
                            <td>{{$run->segments->count()}}</td>
                            <td>{{$run->elapsed_time}}</td>
                            <td><a href="#" class="status" data-type="select" data-pk="{{$run->id}}" data-source="/status/statuses" id="status_id" data-title="Change Run Status" data-value="{{$run->status_id}}" data-url="/status/update-status">{{$run->status->status}}</a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!---Add Run Modal--->
<div id="addModal" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"></button>
                <h4 class="modal-title">New Run</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button id="closeModal" class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                <button id="submitModal" class="btn btn-info" type="button">Save</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('jquery2')
{!! HTML::script('conquer/plugins/data-tables/jquery.dataTables.js') !!}
{!! HTML::script('conquer/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js') !!}
@stop

@section('scripts')
{!! HTML::script('conquer/plugins/data-tables/DT_bootstrap.js') !!}
{!! HTML::script('conquer/js/runs.js') !!}
@stop