{!! Form::open() !!}
<div class="row">
    <div class="col-md-12">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, array('class' => 'form-control', 'id' => 'name')) !!}
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        {!! Form::label('game', 'Game') !!}
        {!! Form::select('game', $games, $game, array('class' => 'form-control', 'id' => 'game')) !!}
    </div>
</div>
{!! Form::close() !!}